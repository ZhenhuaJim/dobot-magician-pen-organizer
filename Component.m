classdef Component
    %COMPONENT Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        model;
        workspace;
    end
    
    methods
        function self = Component(objPath, objName, workspace, base)
            L1 = Link('alpha',0,'a',0,'d',0,'offset',0);
            self.model = SerialLink([L1],'name', objName);
            self.model.base = base;
            
            for linkIndex = 0:self.model.n
                [ faceData, vertexData, plyData{linkIndex+1} ] = plyread(objPath,'tri');
                self.model.faces{linkIndex+1} = faceData;
                self.model.points{linkIndex+1} = vertexData;
            end
            self.workspace = workspace;
            
            self.model.plot3d(zeros(1,self.model.n),'noarrow','workspace',self.workspace);
%             self.model.plot(zeros(1,self.model.n),'workspace',self.workspace);
            for linkIndex = 0:self.model.n
                handles = findobj('Tag', self.model.name);
                h = get(handles,'UserData');
                
                try
                    h.link(linkIndex+1).Children.FaceVertexCData = [plyData{linkIndex+1}.vertex.red ...
                        , plyData{linkIndex+1}.vertex.green ...
                        , plyData{linkIndex+1}.vertex.blue]/255;
                    h.link(linkIndex+1).Children.FaceColor = 'interp';
                    
                catch ME_1
                    disp(ME_1);
                    continue;
                end
            end
            
            
        end
        
        function pos = GetPos(self)
            pos = self.model.base(1:3,4);
        end
        
        function trans = GetTrans(self)
            trans = self.model.base;
        end
    end
end

