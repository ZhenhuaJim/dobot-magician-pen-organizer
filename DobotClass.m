classdef DobotClass < handle
    %DOBOT Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        model;
        workspace;
        name;
        T;
        q;
        % If using realRobot
        realRobot
        % Set pos params
        setPosMsg;
        setPosSrv;
        % Get state Params
        stateMsg;
        stateSub;
        % Set Raw params
        rawMsg;
        rawSrv;
    end
    
    methods
        function self = DobotClass(workspace, name, realRobot)
            self.realRobot = realRobot;
            if(realRobot)
                % Define set pos msg and service
                self.setPosMsg = rosmessage('dobot_magician/SetPosAngRequest');
                self.setPosSrv = rossvcclient('/dobot_magician/joint_angs');
                % Define state msg and subscriber
                self.stateMsg = rosmessage('dobot_magician/State');
                self.stateSub = rossubscriber('/dobot_magician/state');
                % Define raw communication msg and srv
                self.rawMsg = rosmessage('dobot_magician/RawCmdRequest');
                self.rawSrv = rossvcclient('/dobot_magician/raw');
            end
            
            % Initialise the DH parameters of the Dobot
            L1 = Link('d',0.139,'a',0,'alpha',-pi/2,'offset',0,'qlim',deg2rad([-135 135]));
            L2 = Link('d',0,'a',0.135,'alpha',0,'offset',-pi/2,'qlim',deg2rad([5 80]));
            L3 = Link('d',0,'a',0.147,'alpha',0,'offset',0,'qlim',deg2rad([15 170]));
            L4 = Link('d',0,'a',0.06,'alpha',pi/2,'offset',0,'qlim',deg2rad([-85 85]));
            L5 = Link('d',-0.105,'a',0,'alpha',0,'offset',0,'qlim',deg2rad([-85 85]));
            self.model = SerialLink([L1 L2 L3 L4 L5], 'name', name);
            self.workspace = workspace;
            self.q = [0 pi/4 pi/4 0 0];
            if realRobot
                self.sendJointAngle(self.q);
            end
            self.T = self.model.fkine(self.q);
            
            % Initialise the simulation model of the Dobot
            for linkIndex = 0:self.model.n
                [ faceData, vertexData, plyData{linkIndex+1} ] = plyread(['files/dobot/Link',num2str(linkIndex),'_ply.ply'],'tri');
                self.model.faces{linkIndex+1} = faceData;
                self.model.points{linkIndex+1} = vertexData;
            end
            
            self.model.plot3d(self.q,'noarrow','workspace',self.workspace);
            
            
            % Correct color of the dobot
            for linkIndex = 0:self.model.n
                handles = findobj('Tag', self.model.name);
                h = get(handles,'UserData');
                try
                    h.link(linkIndex+1).Children.FaceVertexCData = [plyData{linkIndex+1}.vertex.red ...
                        , plyData{linkIndex+1}.vertex.green ...
                        , plyData{linkIndex+1}.vertex.blue]/255;
                    h.link(linkIndex+1).Children.FaceColor = 'interp';
                catch ME_1
                    disp(ME_1);
                    continue;
                end
            end
        end
        
        
        function Mov2Local(self, Td, steps)
            qCurr = self.q;
            qDest = self.model.ikcon(Td,qCurr);
            qMat = jtraj(qCurr, qDest, steps);
            if steps == 1
                self.model.animate(qDest);
                if self.realRobot
                    self.sendJointAngle(qDest);
                end
                self.T = self.model.fkine(qDest);
                self.q = qDest;
            else
                for i = 1:steps
                    self.model.animate(qMat(i,:));
                    if self.realRobot
                        self.sendJointAngle(qMat(i,:));
                    end
                    self.T = self.model.fkine(qMat(i,:));
                    self.q = qMat(i,:);
                end
            end
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Dobot ROS functions
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function SendJointAngle(self,q)
            q(3) =  q(3) - pi/2 + q(2);
            self.setPosMsg.JointAngles = [q(1), q(2), q(3), q(5)];
            self.setPosSrv.call(self.setPosMsg);
%             pause(0.0001);
        end
        
        function OpenGripper(self)
            self.rawMsg.Len = 4;
            self.rawMsg.ID = 63;
            self.rawMsg.Rw = 1;
            self.rawMsg.IsQ = 0;
            self.rawMsg.Cmd = '10';
            self.rawSrv.call(self.rawMsg);
        end
        
        function closeGripper(self)
            self.rawMsg.Len = 4;
            self.rawMsg.ID = 63;
            self.rawMsg.Rw = 1;
            self.rawMsg.IsQ = 0;
            self.rawMsg.Cmd = '11';
            self.rawSrv.call(self.rawMsg);
        end
        
        function DisableGripper(self)
            self.rawMsg.Len = 4;
            self.rawMsg.ID = 63;
            self.rawMsg.Rw = 1;
            self.rawMsg.IsQ = 0;
            self.rawMsg.Cmd = '00';
            self.rawSrv.call(self.rawMsg);
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function newq = ikcon(self, T, q)
            newq = self.model.ikcon(T,q);
        end
        
        % move the simulation figure
        function animate(self, q)
            self.model.animate(q);
        end
        
        % Plot the robot model in stick figure
        function plot(self, q, scale)
            self.model.plot(q,'workspace',self.workspace,'scale',scale);
        end
        
        % Plot the model in 3d of the dobot magician
        function plot3d(self, q, scale)
            self.model.plot3d(q, 'workspace', self.workspace,'scale',scale);
        end
    end
    
end
