function penMatrix = penDetection()
%This function is to find pens out of an image and finding their 
%distances and locations.
clear all;
clc;

%% Read the image
% I = imread ('C:\Users\Jimzh\Dropbox\1. Subject document\Semester 9 2018\41013 Robotics\Assignment_2\lab_assignment_2/image.jpg'); %change image from folder
webcamlist;

cam = webcam(1);

preview(cam);
I = snapshot(cam);

closePreview(cam);

% while 1
%     I = snapshot(cam);
% 
%     imshow(I);
%     hold on
%     plot(320, 240, 'r+', 'LineWidth', 2, 'MarkerSize', 15);
% end



%% Convert RGB individually to grayscale
rmat = I(:,:,1);
gmat = I(:,:,2);
bmat = I(:,:,3);

figure;
subplot(2,2,1), imshow(rmat);
title('Red Plane');
subplot(2,2,2), imshow(gmat);
title('Green Plane');
subplot(2,2,3), imshow(bmat);
title('Blue Plane');
subplot(2,2,4), imshow(I);
title('Original Plane');

%% sets the level of intensity by colour and combine them
levelr = 0.45;   %0.6
levelg = 0.35;   %0.4
levelb = 0.15;  %0.15
I1=im2bw(rmat,levelr);
I2=im2bw(gmat,levelg);
I3=im2bw(bmat,levelb);
Isum = (I1&I2&I3);
% 
subplot(2,2,1), imshow(I1);
title('Red Plane');
subplot(2,2,2), imshow(I2);
title('Green Plane');
subplot(2,2,3), imshow(I3);
title('Blue Plane');
subplot(2,2,4), imshow(Isum);
title('Sum of all Plane');

%% Complement image and Fill in holes
Icomp = imcomplement(Isum);
Ifilled = imfill(Icomp, 'holes');
% figure, imshow(Ifilled);

%%
se = strel('disk', 5);
Iopenned = imopen(Ifilled,se);
% imshow(Iopenned);

%% Extract features

Iregion = regionprops(Iopenned, 'centroid');
[labeled,numObjects] = bwlabel(Iopenned, 4);
stats = regionprops(labeled, 'Centroid','Orientation','Eccentricity','Area','BoundingBox');
areas = [stats.Area];
centre = reshape([stats.Centroid], 2,[])
orientation = [stats.Orientation]
posMatrix = [centre;orientation]
eccentricities = [stats.Eccentricity];

%% Use feature analysis to count the number of pens
IdxOfPens = find(eccentricities);
statsDefects = stats(IdxOfPens);

figure, imshow(I);
hold on;
i = 320;
j = 240;
for Idx = 1 : length(IdxOfPens)
    h = rectangle('Position', statsDefects(Idx).BoundingBox);
    set(h,'EdgeColor',[.75 0 0]);
    plot(i, j, 'r+', 'LineWidth', 2, 'MarkerSize', 15);
    hold on;
end
if Idx >= 1;
    title(['There are ', num2str(numObjects), ' objects in the picture']);
end


%% Position to the camera frame

Z = 0.32;

%Calculate principal point
[x1,y1,z1] = size(I);
u0 = y1/2
v0 = x1/2

%focal length and camera angle
CamAngle = deg2rad(45.2); %in rad

f = 320*cot(CamAngle/2)

%Create a matrix of all the pen location and yaw
penMatrix = zeros(4,size(posMatrix,2));

for Pen = 1 : size(posMatrix,2)
    x = posMatrix(1,Pen);
    y = posMatrix(2,Pen);

    X = (((x-u0)*Z)/f)*1.25;
    Y = (((y-v0)*Z)/f)-0.27;
    
    % X and Y reversed in the matrix to adapt easily to the global frame !
    
    penMatrix(1,Pen) = -Y;
    penMatrix(2,Pen) = -X;
    penMatrix(3,Pen) = Z;
    
    if posMatrix(3,Pen) >= 0
        theta = posMatrix(3,Pen) - 90;
    else
        theta = posMatrix(3,Pen) + 90;
    end
    
    penMatrix(4,Pen) = deg2rad(theta);
    
    p = plot(x, y, 'r+', 'LineWidth', 2, 'MarkerSize', 15);
    hold on;
end
end
