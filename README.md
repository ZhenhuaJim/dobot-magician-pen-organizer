## Dobot Magician Pick and Place Control and Simulation

The project is develop for robotics subject. The system utilised Dobot Magician and Microsoft LiveCam to extract pen location from image.

ROS and Matlab are used to develop the algorithm and interface with the Dobot Magician. The grapical user interface is developed using Matlab internal GUI development program.

To run the simulation:

1. Run startup_rvc.m under **rvctools** to add dependencies to Matlab path.
2. Edit **Gui.m**
3. Change line 58 variable **realRobot** to 0.
4. Run **Gui.m** to initialise graphical user interface.

System demonstration and explanation video:
https://youtu.be/73WCzwqb9w8
