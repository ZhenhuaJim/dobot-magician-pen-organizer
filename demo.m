function demo()
clc
close all
clear

L1 = Link('d',0.139,'a',0,'alpha',-pi/2,'offset',0,'qlim',deg2rad([-135 135]));
L2 = Link('d',0,'a',0.135,'alpha',0,'offset',deg2rad(-90),'qlim',deg2rad([5 80]));
L3 = Link('d',0,'a',0.147,'alpha',0,'offset',deg2rad(0),'qlim',deg2rad([15 170]));
L4 = Link('d',0,'a',0.01,'alpha',pi/2,'offset',0)%,'qlim',deg2rad([-90 90]));
L5 = Link('d',0.01,'a',0,'alpha',0,'offset',0);

q = zeros(1,5);

model = SerialLink([L1 L2 L3 L4 L5], 'name', 'quack');
workspace = [-1 1 -1 1 -1 1];

model.teach()

scale = 0.3;
model.plot(q,'workspace',workspace,'scale',scale);
demoData = load('dobot_q.mat');
demoData = [demoData.dobot_q zeros(338,1)];
data = rad2deg(demoData);

pause;

for i = 1:338
    q3 =  pi/2 - demoData(i,2) + demoData(i,3); 
    demoData(i,3) = q3;
    demoData(i,4) = pi/2 - demoData(i,2) - demoData(i,3);
    rad2deg(demoData(i,:))
    demoData(i,:)
    model.animate(demoData(i,:));
end

end