function varargout = gui(varargin)
%GUI MATLAB code file for gui.fig
%      GUI, by itself, creates a new GUI or raises the existing
%      singleton*.
%
%      H = GUI returns the handle to a new GUI or the handle to
%      the existing singleton*.
%
%      GUI('Property','Value',...) creates a new GUI using the
%      given property value pairs. Unrecognized properties are passed via
%      varargin to gui_OpeningFcn.  This calling syntax produces a
%      warning when there is an existing singleton*.
%
%      GUI('CALLBACK') and GUI('CALLBACK',hObject,...) call the
%      local function named CALLBACK in GUI.M with the given input
%      arguments.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help gui

% Last Modified by GUIDE v2.5 07-Oct-2018 09:22:50

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @gui_OpeningFcn, ...
                   'gui_OutputFcn',  @gui_OutputFcn, ...
                   'gui_LayoutFcn',  [], ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
   gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before gui is made visible.
function gui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   unrecognized PropertyName/PropertyValue pairs from the
%            command line (see VARARGIN)
rosshutdown;

% If using real robot then 1, if not 0
realRobot = 0;

if realRobot
    rosinit('192.168.0.50');
end
workspace = [-0.3 0.7 -0.5 0.5 -0.01 0.5];
camlight;
axes(handles.simulation);
handles.dobot = DobotClass(workspace, 'dobot_magician', realRobot);
handles.table = Component('files/components/heavyworkbench.ply','bench', workspace, transl(0.2,0,0));
handles.penBox = Component('files/components/penBox.ply','penBox', workspace, transl(0.12,0.25,0));
handles.enclosure = Component('files/components/enclosure.ply','enclosure', workspace, transl(-0.1,0,0));
% Different status
handles.autoStatus = 0;
handles.manualStatus = 0;
handles.goStatus = 0;
handles.stopStatus = 0;
handles.output = hObject;

% Slider default values
set(handles.q1Slider,'Value',handles.dobot.q(1));
set(handles.q2Slider,'Value',handles.dobot.q(2));
set(handles.q3Slider,'Value',handles.dobot.q(3));
set(handles.q4Slider,'Value',handles.dobot.q(5));

% Input text box default values
set(handles.xInput,'string', num2str(handles.dobot.T(1,4)));
set(handles.yInput,'string', num2str(handles.dobot.T(2,4)));
set(handles.zInput,'string', num2str(handles.dobot.T(3,4)));
set(handles.q1Input,'string', num2str(handles.dobot.q(1)));
set(handles.q2Input,'string', num2str(handles.dobot.q(2)));
set(handles.q3Input,'string', num2str(handles.dobot.q(3)));
set(handles.q4Input,'string', num2str(handles.dobot.q(5)));
% Dobot status
handles.gripperStatus = 0;
guidata(hObject, handles);

% UIWAIT makes gui wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = gui_OutputFcn(hObject, eventdata, handles)

% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on slider movement.
function q1Slider_Callback(hObject, eventdata, handles)
% hObject    handle to q1Slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~get(handles.stopButton,'Value')
    handles.dobot.q(1) = get(hObject,'Value');
    if handles.dobot.realRobot
        handles.dobot.SendJointAngle(handles.dobot.q);
    end
    handles.dobot.T = handles.dobot.model.fkine(handles.dobot.q);
    set(handles.q2Slider,'Value',handles.dobot.q(2));
    set(handles.q3Slider,'Value',handles.dobot.q(3));
    set(handles.q4Slider,'Value',handles.dobot.q(5));
    set(handles.xInput,'string', num2str(handles.dobot.T(1,4)));
    set(handles.yInput,'string', num2str(handles.dobot.T(2,4)));
    set(handles.zInput,'string', num2str(handles.dobot.T(3,4)));
    set(handles.q1Input,'string', num2str(handles.dobot.q(1)));
    set(handles.q2Input,'string', num2str(handles.dobot.q(2)));
    set(handles.q3Input,'string', num2str(handles.dobot.q(3)));
    set(handles.q4Input,'string', num2str(handles.dobot.q(5)));
    guidata(hObject, handles);
    handles.dobot.animate(handles.dobot.q);
end
% handles.dobot.SendJointAngle(handles.currentQ);
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function q1Slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q1Slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function q1Input_Callback(hObject, eventdata, handles)
% hObject    handle to q1Input (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~get(handles.stopButton,'Value')
    handles.dobot.q(1) = str2double(get(hObject,'String'));
    handles.dobot.q(4) = pi/2 - handles.dobot.q(2) - handles.dobot.q(3);
    if handles.dobot.realRobot
        handles.dobot.SendJointAngle(handles.dobot.q);
    end
    handles.dobot.T = handles.dobot.model.fkine(handles.dobot.q);
    set(handles.q1Slider,'Value',handles.dobot.q(1));
    set(handles.q2Slider,'Value',handles.dobot.q(2));
    set(handles.q3Slider,'Value',handles.dobot.q(3));
    set(handles.q4Slider,'Value',handles.dobot.q(5));
    set(handles.xInput,'string', num2str(handles.dobot.T(1,4)));
    set(handles.yInput,'string', num2str(handles.dobot.T(2,4)));
    set(handles.zInput,'string', num2str(handles.dobot.T(3,4)));
    set(handles.q2Input,'string', num2str(handles.dobot.q(2)));
    set(handles.q3Input,'string', num2str(handles.dobot.q(3)));
    set(handles.q4Input,'string', num2str(handles.dobot.q(5)));
    handles.dobot.animate(handles.dobot.q);
    guidata(hObject, handles);
end


% Hints: get(hObject,'String') returns contents of q1Input as text
%        str2double(get(hObject,'String')) returns contents of q1Input as a double


% --- Executes during object creation, after setting all properties.
function q1Input_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q1Input (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function q2Input_Callback(hObject, eventdata, handles)
% hObject    handle to q2Input (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~get(handles.stopButton,'Value')
    handles.dobot.q(2) = str2double(get(hObject,'String'));
    handles.dobot.q(4) = pi/2 - handles.dobot.q(2) - handles.dobot.q(3);
    if handles.dobot.realRobot
        handles.dobot.SendJointAngle(handles.dobot.q);
    end
    handles.dobot.T = handles.dobot.model.fkine(handles.dobot.q);
    set(handles.q1Slider,'Value',handles.dobot.q(1));
    set(handles.q2Slider,'Value',handles.dobot.q(2));
    set(handles.q3Slider,'Value',handles.dobot.q(3));
    set(handles.q4Slider,'Value',handles.dobot.q(5));
    set(handles.xInput,'string', num2str(handles.dobot.T(1,4)));
    set(handles.yInput,'string', num2str(handles.dobot.T(2,4)));
    set(handles.zInput,'string', num2str(handles.dobot.T(3,4)));
    set(handles.q1Input,'string', num2str(handles.dobot.q(1)));
    set(handles.q3Input,'string', num2str(handles.dobot.q(3)));
    set(handles.q4Input,'string', num2str(handles.dobot.q(5)));
    handles.dobot.animate(handles.dobot.q);
    guidata(hObject, handles);
end
% Hints: get(hObject,'String') returns contents of q2Input as text
%        str2double(get(hObject,'String')) returns contents of q2Input as a double


% --- Executes during object creation, after setting all properties.
function q2Input_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q2Input (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function q3Input_Callback(hObject, eventdata, handles)
% hObject    handle to q3Input (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~get(handles.stopButton,'Value')
    handles.dobot.q(3) = str2double(get(hObject,'String'));
    handles.dobot.q(4) = pi/2 - handles.dobot.q(2) - handles.dobot.q(3);
    handles.dobot.T = handles.dobot.model.fkine(handles.dobot.q);
    if handles.dobot.realRobot
        handles.dobot.SendJointAngle(handles.dobot.q);
    end
    set(handles.q1Slider,'Value',handles.dobot.q(1));
    set(handles.q2Slider,'Value',handles.dobot.q(2));
    set(handles.q3Slider,'Value',handles.dobot.q(3));
    set(handles.q4Slider,'Value',handles.dobot.q(5));
    set(handles.xInput,'string', num2str(handles.dobot.T(1,4)));
    set(handles.yInput,'string', num2str(handles.dobot.T(2,4)));
    set(handles.zInput,'string', num2str(handles.dobot.T(3,4)));
    set(handles.q1Input,'string', num2str(handles.dobot.q(1)));
    set(handles.q2Input,'string', num2str(handles.dobot.q(2)));
    set(handles.q4Input,'string', num2str(handles.dobot.q(5)));
    handles.dobot.animate(handles.dobot.q);
    guidata(hObject, handles);
end
% Hints: get(hObject,'String') returns contents of q3Input as text
%        str2double(get(hObject,'String')) returns contents of q3Input as a double


% --- Executes during object creation, after setting all properties.
function q3Input_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q3Input (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function q4Input_Callback(hObject, eventdata, handles)
% hObject    handle to q4Input (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~get(handles.stopButton,'Value')
    handles.dobot.q(5) = str2double(get(hObject,'String'));
    handles.dobot.q(4) = pi/2 - handles.dobot.q(2) - handles.dobot.q(3);
    handles.dobot.T = handles.dobot.model.fkine(handles.dobot.q);
    if handles.dobot.realRobot
        handles.dobot.SendJointAngle(handles.dobot.q);
    end
    set(handles.q1Slider,'Value',handles.dobot.q(1));
    set(handles.q2Slider,'Value',handles.dobot.q(2));
    set(handles.q3Slider,'Value',handles.dobot.q(3));
    set(handles.q4Slider,'Value',handles.dobot.q(5));
    set(handles.xInput,'string', num2str(handles.dobot.T(1,4)));
    set(handles.yInput,'string', num2str(handles.dobot.T(2,4)));
    set(handles.zInput,'string', num2str(handles.dobot.T(3,4)));
    set(handles.q1Input,'string', num2str(handles.dobot.q(1)));
    set(handles.q2Input,'string', num2str(handles.dobot.q(2)));
    set(handles.q3Input,'string', num2str(handles.dobot.q(3)));
    handles.dobot.animate(handles.dobot.q);
    guidata(hObject, handles);
end
% Hints: get(hObject,'String') returns contents of q4Input as text
%        str2double(get(hObject,'String')) returns contents of q4Input as a double


% --- Executes during object creation, after setting all properties.
function q4Input_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q4Input (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function q2Slider_Callback(hObject, eventdata, handles)
% hObject    handle to q2Slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~get(handles.stopButton,'Value')
    handles.dobot.q(2) = get(hObject,'Value');
    % handles.dobot.SendJointAngle(q);
    handles.dobot.q(4) = pi/2 - handles.dobot.q(2) - handles.dobot.q(3);

    if handles.dobot.realRobot
        handles.dobot.SendJointAngle(handles.dobot.q);
    end

    handles.dobot.T = handles.dobot.model.fkine(handles.dobot.q);
    set(handles.q1Slider,'Value',handles.dobot.q(1));
    set(handles.q3Slider,'Value',handles.dobot.q(3));
    set(handles.q4Slider,'Value',handles.dobot.q(5));
    set(handles.xInput,'string', num2str(handles.dobot.T(1,4)));
    set(handles.yInput,'string', num2str(handles.dobot.T(2,4)));
    set(handles.zInput,'string', num2str(handles.dobot.T(3,4)));
    set(handles.q1Input,'string', num2str(handles.dobot.q(1)));
    set(handles.q2Input,'string', num2str(handles.dobot.q(2)));
    set(handles.q3Input,'string', num2str(handles.dobot.q(3)));
    set(handles.q4Input,'string', num2str(handles.dobot.q(5)));
    guidata(hObject, handles);
    handles.dobot.animate(handles.dobot.q);
end


% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function q2Slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q2Slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function q3Slider_Callback(hObject, eventdata, handles)
% hObject    handle to q3Slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~get(handles.stopButton,'Value')
    handles.dobot.q(3) = get(hObject,'Value');
    % handles.dobot.SendJointAngle(q);
    handles.dobot.q(4) = pi/2 - handles.dobot.q(2) - handles.dobot.q(3);
    handles.dobot.T = handles.dobot.model.fkine(handles.dobot.q);
    if handles.dobot.realRobot
        handles.dobot.SendJointAngle(handles.dobot.q);
    end
    set(handles.q1Slider,'Value',handles.dobot.q(1));
    set(handles.q2Slider,'Value',handles.dobot.q(2));
    set(handles.q4Slider,'Value',handles.dobot.q(5));
    set(handles.xInput,'string', num2str(handles.dobot.T(1,4)));
    set(handles.yInput,'string', num2str(handles.dobot.T(2,4)));
    set(handles.zInput,'string', num2str(handles.dobot.T(3,4)));
    set(handles.q1Input,'string', num2str(handles.dobot.q(1)));
    set(handles.q2Input,'string', num2str(handles.dobot.q(2)));
    set(handles.q3Input,'string', num2str(handles.dobot.q(3)));
    set(handles.q4Input,'string', num2str(handles.dobot.q(5)));

    guidata(hObject, handles);

    handles.dobot.animate(handles.dobot.q);
end

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function q3Slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q3Slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function q4Slider_Callback(hObject, eventdata, handles)
% hObject    handle to q4Slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~get(handles.stopButton,'Value')
    handles.dobot.q(5) = get(hObject,'Value');
    handles.dobot.q(4) = pi/2 - handles.dobot.q(2) - handles.dobot.q(3);
    if handles.dobot.realRobot
        handles.dobot.SendJointAngle(handles.dobot.q);
    end
    handles.dobot.T = handles.dobot.model.fkine(handles.dobot.q);
    set(handles.q1Slider,'Value',handles.dobot.q(1));
    set(handles.q2Slider,'Value',handles.dobot.q(2));
    set(handles.q3Slider,'Value',handles.dobot.q(3));
    set(handles.xInput,'string', num2str(handles.dobot.T(1,4)));
    set(handles.yInput,'string', num2str(handles.dobot.T(2,4)));
    set(handles.zInput,'string', num2str(handles.dobot.T(3,4)));
    set(handles.q1Input,'string', num2str(handles.dobot.q(1)));
    set(handles.q2Input,'string', num2str(handles.dobot.q(2)));
    set(handles.q3Input,'string', num2str(handles.dobot.q(3)));
    set(handles.q4Input,'string', num2str(handles.dobot.q(5)));
    guidata(hObject, handles);

    handles.dobot.animate(handles.dobot.q);
end
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function q4Slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q4Slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in stopButton.
function stopButton_Callback(hObject, eventdata, handles)
% hObject    handle to stopButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.dobot.DisableGripper();
h = get(hObject,'Value');
guidata(hObject, handles);

% --- Executes on button press in manualOnOff.
function manualOnOff_Callback(hObject, eventdata, handles)
% hObject    handle to manualOnOff (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of manualOnOff


% --- Executes on button press in xMinus.
function xMinus_Callback(hObject, eventdata, handles)
% hObject    handle to xMinus (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~get(handles.stopButton,'Value')
    if (((handles.dobot.T(1,4)-0.005)^2+(handles.dobot.T(2,4))^2) < 0.12^2)
        ;
    else
        handles.dobot.T(1,4) = handles.dobot.T(1,4) - 0.005;
        handles.dobot.Mov2Local(handles.dobot.T,1);
    end
    set(handles.q1Slider,'Value',handles.dobot.q(1));
    set(handles.q2Slider,'Value',handles.dobot.q(2));
    set(handles.q3Slider,'Value',handles.dobot.q(3));
    set(handles.q4Slider,'Value',handles.dobot.q(5));
    set(handles.xInput,'string', num2str(handles.dobot.T(1,4)));
    set(handles.yInput,'string', num2str(handles.dobot.T(2,4)));
    set(handles.zInput,'string', num2str(handles.dobot.T(3,4)));
    set(handles.q1Input,'string', num2str(handles.dobot.q(1)));
    set(handles.q2Input,'string', num2str(handles.dobot.q(2)));
    set(handles.q3Input,'string', num2str(handles.dobot.q(3)));
    set(handles.q4Input,'string', num2str(handles.dobot.q(5)));
    guidata(hObject, handles);
end

% --- Executes on button press in yMinus.
function yMinus_Callback(hObject, eventdata, handles)
% hObject    handle to yMinus (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~get(handles.stopButton,'Value')
    if (((handles.dobot.T(1,4))^2+(handles.dobot.T(2,4)-0.005)^2) < 0.12^2)
        ;
    else
        handles.dobot.T(2,4) = handles.dobot.T(2,4) - 0.005;
        handles.dobot.Mov2Local(handles.dobot.T,1);
    end
    set(handles.q1Slider,'Value',handles.dobot.q(1));
    set(handles.q2Slider,'Value',handles.dobot.q(2));
    set(handles.q3Slider,'Value',handles.dobot.q(3));
    set(handles.q4Slider,'Value',handles.dobot.q(5));
    set(handles.xInput,'string', num2str(handles.dobot.T(1,4)));
    set(handles.yInput,'string', num2str(handles.dobot.T(2,4)));
    set(handles.zInput,'string', num2str(handles.dobot.T(3,4)));
    set(handles.q1Input,'string', num2str(handles.dobot.q(1)));
    set(handles.q2Input,'string', num2str(handles.dobot.q(2)));
    set(handles.q3Input,'string', num2str(handles.dobot.q(3)));
    set(handles.q4Input,'string', num2str(handles.dobot.q(5)));
    guidata(hObject, handles);
end


% --- Executes on button press in zMinus.
function zMinus_Callback(hObject, eventdata, handles)
% hObject    handle to zMinus (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~get(handles.stopButton,'Value')
    if (handles.dobot.T(3,4)-0.005 < 0)
        ;
    else
        handles.dobot.T(3,4) = handles.dobot.T(3,4) - 0.005;
        handles.dobot.Mov2Local(handles.dobot.T,1);
    end
    set(handles.q1Slider,'Value',handles.dobot.q(1));
    set(handles.q2Slider,'Value',handles.dobot.q(2));
    set(handles.q3Slider,'Value',handles.dobot.q(3));
    set(handles.q4Slider,'Value',handles.dobot.q(5));
    set(handles.xInput,'string', num2str(handles.dobot.T(1,4)));
    set(handles.yInput,'string', num2str(handles.dobot.T(2,4)));
    set(handles.zInput,'string', num2str(handles.dobot.T(3,4)));
    set(handles.q1Input,'string', num2str(handles.dobot.q(1)));
    set(handles.q2Input,'string', num2str(handles.dobot.q(2)));
    set(handles.q3Input,'string', num2str(handles.dobot.q(3)));
    set(handles.q4Input,'string', num2str(handles.dobot.q(5)));
    guidata(hObject, handles);
end


% --- Executes on button press in yawMinus.
function yawMinus_Callback(hObject, eventdata, handles)
% hObject    handle to yawMinus (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in xPlus.
function xPlus_Callback(hObject, eventdata, handles)
% hObject    handle to xPlus (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~get(handles.stopButton,'Value')
    if (((handles.dobot.T(1,4)+0.005)^2+(handles.dobot.T(2,4))^2) > 0.32^2)
        ;
    else
        handles.dobot.T(1,4) = handles.dobot.T(1,4) + 0.005;
        handles.dobot.Mov2Local(handles.dobot.T,1);
    end
    set(handles.q1Slider,'Value',handles.dobot.q(1));
    set(handles.q2Slider,'Value',handles.dobot.q(2));
    set(handles.q3Slider,'Value',handles.dobot.q(3));
    set(handles.q4Slider,'Value',handles.dobot.q(5));
    set(handles.xInput,'string', num2str(handles.dobot.T(1,4)));
    set(handles.yInput,'string', num2str(handles.dobot.T(2,4)));
    set(handles.zInput,'string', num2str(handles.dobot.T(3,4)));
    set(handles.q1Input,'string', num2str(handles.dobot.q(1)));
    set(handles.q2Input,'string', num2str(handles.dobot.q(2)));
    set(handles.q3Input,'string', num2str(handles.dobot.q(3)));
    set(handles.q4Input,'string', num2str(handles.dobot.q(5)));
    guidata(hObject, handles);
end


% --- Executes on button press in yPlus.
function yPlus_Callback(hObject, eventdata, handles)
% hObject    handle to yPlus (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~get(handles.stopButton,'Value')
    if (((handles.dobot.T(1,4))^2+(handles.dobot.T(2,4)+0.005)^2) > 0.32^2)
        ;
    else
        handles.dobot.T(2,4) = handles.dobot.T(2,4) + 0.005;
        handles.dobot.Mov2Local(handles.dobot.T,1);
    end
    set(handles.q1Slider,'Value',handles.dobot.q(1));
    set(handles.q2Slider,'Value',handles.dobot.q(2));
    set(handles.q3Slider,'Value',handles.dobot.q(3));
    set(handles.q4Slider,'Value',handles.dobot.q(5));
    set(handles.xInput,'string', num2str(handles.dobot.T(1,4)));
    set(handles.yInput,'string', num2str(handles.dobot.T(2,4)));
    set(handles.zInput,'string', num2str(handles.dobot.T(3,4)));
    set(handles.q1Input,'string', num2str(handles.dobot.q(1)));
    set(handles.q2Input,'string', num2str(handles.dobot.q(2)));
    set(handles.q3Input,'string', num2str(handles.dobot.q(3)));
    set(handles.q4Input,'string', num2str(handles.dobot.q(5)));
    guidata(hObject, handles);
end


% --- Executes on button press in zPlus.
function zPlus_Callback(hObject, eventdata, handles)
% hObject    handle to zPlus (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~get(handles.stopButton,'Value')
    if (handles.dobot.T(3,4)+0.005 > 0.2)
        ;
    else
        handles.dobot.T(3,4) = handles.dobot.T(3,4) + 0.005;
        handles.dobot.Mov2Local(handles.dobot.T,1);
    end
    set(handles.q1Slider,'Value',handles.dobot.q(1));
    set(handles.q2Slider,'Value',handles.dobot.q(2));
    set(handles.q3Slider,'Value',handles.dobot.q(3));
    set(handles.q4Slider,'Value',handles.dobot.q(5));
    set(handles.xInput,'string', num2str(handles.dobot.T(1,4)));
    set(handles.yInput,'string', num2str(handles.dobot.T(2,4)));
    set(handles.zInput,'string', num2str(handles.dobot.T(3,4)));
    set(handles.q1Input,'string', num2str(handles.dobot.q(1)));
    set(handles.q2Input,'string', num2str(handles.dobot.q(2)));
    set(handles.q3Input,'string', num2str(handles.dobot.q(3)));
    set(handles.q4Input,'string', num2str(handles.dobot.q(5)));
    guidata(hObject, handles);
end

% --- Executes on button press in yawPlus.
function yawPlus_Callback(hObject, eventdata, handles)
% hObject    handle to yawPlus (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function xInput_Callback(hObject, eventdata, handles)
% hObject    handle to xInput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~get(handles.stopButton,'Value')
    T = handles.dobot.T;
    T(1,4) = str2double(get(hObject,'String'))
    x = ((T(1,4))^2+(T(2,4))^2);
    if ((x < 0.12^2) || (x > 0.32^2))
        ;
    else
        handles.dobot.Mov2Local(T,1);
        handles.dobot.animate(handles.dobot.q);
    end
    set(handles.q1Slider,'Value',handles.dobot.q(1));
    set(handles.q2Slider,'Value',handles.dobot.q(2));
    set(handles.q3Slider,'Value',handles.dobot.q(3));
    set(handles.q4Slider,'Value',handles.dobot.q(5));
    set(handles.yInput,'string', num2str(handles.dobot.T(2,4)));
    set(handles.zInput,'string', num2str(handles.dobot.T(3,4)));
    set(handles.q1Input,'string', num2str(handles.dobot.q(1)));
    set(handles.q2Input,'string', num2str(handles.dobot.q(2)));
    set(handles.q3Input,'string', num2str(handles.dobot.q(3)));
    set(handles.q4Input,'string', num2str(handles.dobot.q(5)));
end
% Hints: get(hObject,'String') returns contents of xInput as text
%        str2double(get(hObject,'String')) returns contents of xInput as a double


% --- Executes during object creation, after setting all properties.
function xInput_CreateFcn(hObject, eventdata, handles)
% hObject    handle to xInput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function yInput_Callback(hObject, eventdata, handles)
% hObject    handle to yInput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~get(handles.stopButton,'Value')
    T = handles.dobot.T;
    T(2,4) = str2double(get(hObject,'String'));
    x = ((T(1,4))^2+(T(2,4))^2);
    if ((x < 0.12^2) || (x > 0.32^2))
        ;
    else
        handles.dobot.Mov2Local(T,1);
        handles.dobot.animate(handles.dobot.q);
    end
    set(handles.q1Slider,'Value',handles.dobot.q(1));
    set(handles.q2Slider,'Value',handles.dobot.q(2));
    set(handles.q3Slider,'Value',handles.dobot.q(3));
    set(handles.q4Slider,'Value',handles.dobot.q(5));
    set(handles.yInput,'string', num2str(handles.dobot.T(2,4)));
    set(handles.zInput,'string', num2str(handles.dobot.T(3,4)));
    set(handles.q1Input,'string', num2str(handles.dobot.q(1)));
    set(handles.q2Input,'string', num2str(handles.dobot.q(2)));
    set(handles.q3Input,'string', num2str(handles.dobot.q(3)));
    set(handles.q4Input,'string', num2str(handles.dobot.q(5)));
end
% Hints: get(hObject,'String') returns contents of yInput as text
%        str2double(get(hObject,'String')) returns contents of yInput as a double


% --- Executes during object creation, after setting all properties.
function yInput_CreateFcn(hObject, eventdata, handles)
% hObject    handle to yInput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function zInput_Callback(hObject, eventdata, handles)
% hObject    handle to zInput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~get(handles.stopButton,'Value')
    T = handles.dobot.T;
    T(3,4) = str2double(get(hObject,'String'));
    x = ((T(1,4))^2+(T(2,4))^2);
    if ((x < 0.12^2) || (x > 0.32^2))
        ;
    else
        handles.dobot.Mov2Local(T,1);
        handles.dobot.animate(handles.dobot.q);
    end
    set(handles.q1Slider,'Value',handles.dobot.q(1));
    set(handles.q2Slider,'Value',handles.dobot.q(2));
    set(handles.q3Slider,'Value',handles.dobot.q(3));
    set(handles.q4Slider,'Value',handles.dobot.q(5));
    set(handles.yInput,'string', num2str(handles.dobot.T(2,4)));
    set(handles.xInput,'string', num2str(handles.dobot.T(1,4)));
    set(handles.q1Input,'string', num2str(handles.dobot.q(1)));
    set(handles.q2Input,'string', num2str(handles.dobot.q(2)));
    set(handles.q3Input,'string', num2str(handles.dobot.q(3)));
    set(handles.q4Input,'string', num2str(handles.dobot.q(5)));
end
% Hints: get(hObject,'String') returns contents of zInput as text
%        str2double(get(hObject,'String')) returns contents of zInput as a double


% --- Executes during object creation, after setting all properties.
function zInput_CreateFcn(hObject, eventdata, handles)
% hObject    handle to zInput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function yawInput_Callback(hObject, eventdata, handles)
% hObject    handle to yawInput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of yawInput as text
%        str2double(get(hObject,'String')) returns contents of yawInput as a double


% --- Executes during object creation, after setting all properties.
function yawInput_CreateFcn(hObject, eventdata, handles)
% hObject    handle to yawInput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in gripStatus.
function gripStatus_Callback(hObject, eventdata, handles)
if handles.gripperStatus == 0
    handles.gripperStatus = 1;
    handles.dobot.OpenGripper();
else
    handles.gripperStatus = 0;
    handles.dobot.CloseGripper();
end
guidata(hObject, handles);


% --- Executes on button press in automaticOnOff.
function automaticOnOff_Callback(hObject, eventdata, handles)
% hObject    handle to automaticOnOff (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    if get(hObject,'Value') ~= 1
        return;
    end
%     pens = [0.261 0.05 0.0036 -0.928;
%         0.26 -0.049 0.0036 0;
%         0.26 -0.12 0.0036 0.9;];
    Tr = transl(0.21,0,0.2);
    handles.dobot.Mov2Local(Tr, 1);
    axes(handles.liveFeed);
    pens = PenDetection();
    pens = pens';
%     imshow(image);
    
    handles.dobot.OpenGripper();
    fprintf('await for GO \n');
    
    while ~get(handles.automaticGo, 'Value')
        pause(0.01);
    end
    
    [num, col] = size(pens);
    for i = 1:num
        if get(handles.stopButton,'Value')
            return;
        end
    % Move to move to top of box
        steps = 1;
        Tr = transl(pens(i,1),pens(i,2),0.15)*trotz(pens(i,4));
        handles.dobot.Mov2Local(Tr, steps);
        handles.dobot.OpenGripper();
        pause(1);
        
        if get(handles.stopButton,'Value')
            return;
        end
        
        Tr = transl(pens(i,1),pens(i,2),0.005)*trotz(pens(i,4));
        handles.dobot.Mov2Local(Tr, steps);
        handles.dobot.CloseGripper();
        pause(1);
        
        if get(handles.stopButton,'Value')
            return;
        end
        
        Tr = transl(pens(i,1),pens(i,2),0.15)*trotz(pens(i,4));
        handles.dobot.Mov2Local(Tr, steps);
        handles.dobot.CloseGripper();
        pause(1);
        
        if get(handles.stopButton,'Value')
            return;
        end
        
        Tr = handles.penBox.GetTrans();
        Tr(3,4) = 0.12;
        handles.dobot.Mov2Local(Tr, steps);
        pause(1);
        
        if get(handles.stopButton,'Value')
            return;
        end
        
        k = 0.01*i;
        % Place to the box
        Tr = handles.penBox.GetTrans();
        Tr(2,4) = Tr(2,4) - k;
        Tr(3,4) = 0.02;
        handles.dobot.Mov2Local(Tr, steps);
        handles.dobot.OpenGripper();
        pause(1);
        
        if get(handles.stopButton,'Value')
            return;
        end
        
        % Return to top of box
        Tr = handles.penBox.GetTrans();
        Tr(3,4) = 0.15;
        handles.dobot.Mov2Local(Tr, steps);
        pause(1);
        
        if get(handles.stopButton,'Value')
            return;
        end
        
        Tr = transl(0.21,0,0.2);
        handles.dobot.Mov2Local(Tr, steps);
%         handles.dobot.OpenGripper();
        pause(1);
    end

guidata(hObject, handles);
% Hint: get(hObject,'Value') returns toggle state of automaticOnOff


% --- Executes on button press in automaticGo.
function automaticGo_Callback(hObject, eventdata, handles)
get(hObject,'Value')
guidata(hObject, handles);
% hObject    handle to automaticGo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in resetPose.
function resetPose_Callback(hObject, eventdata, handles)
% hObject    handle to resetPose (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% dobot
handles.dobot.q = [0 pi/4 pi/4 0 0];
handles.dobot.T = handles.dobot.model.fkine(handles.dobot.q);
handles.dobot.Mov2Local(handles.dobot.T, 1)
set(handles.q1Slider,'Value',handles.dobot.q(1));
set(handles.q2Slider,'Value',handles.dobot.q(2));
set(handles.q3Slider,'Value',handles.dobot.q(3));
set(handles.q4Slider,'Value',handles.dobot.q(5));
set(handles.yInput,'string', num2str(handles.dobot.T(2,4)));
set(handles.xInput,'string', num2str(handles.dobot.T(1,4)));
set(handles.zInput,'string', num2str(handles.dobot.T(3,4)));
set(handles.q1Input,'string', num2str(handles.dobot.q(1)));
set(handles.q2Input,'string', num2str(handles.dobot.q(2)));
set(handles.q3Input,'string', num2str(handles.dobot.q(3)));
set(handles.q4Input,'string', num2str(handles.dobot.q(5)));
handles.dobot.animate(handles.dobot.q);
guidata(hObject, handles);
